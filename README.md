# README #

SocialClub - A Instagram Clone

Features:

* Du kannst dich mit deinem eigenen Account einloggen.
* So lange du dich nicht ausloggst wirst automatisch wieder eingeloggt.
* Du kannst dir den Social Club feedansehen und die Posts liken.
* Auserdem kannst du mit deinem Namen eigene Bilder uploaden und mit einer Beschreibung versehen.


Hier siehst du den Log In Bereich und den Social Club Feed.

![Scheme](https://user-images.githubusercontent.com/26789502/28786739-66e90446-761a-11e7-8907-9ef6642130fd.jpg)


Im Add Image Bereich kannst du Deinen Post vorbereiten und hinaus in die Welt schicken.
![Scheme](https://user-images.githubusercontent.com/26789502/28786738-66e1fe3a-761a-11e7-863e-988a56ab4d78.jpg)
