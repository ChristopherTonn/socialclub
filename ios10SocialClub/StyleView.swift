//
//  StyleView.swift
//  ios10SocialClub
//
//  Created by Student on 03.04.17.
//  Copyright © 2017 Student. All rights reserved.
//

import UIKit

class StyleView: UIView {

   
    override func awakeFromNib() {
        
        super.awakeFromNib()
        shadow()
     }
    
    
    //Setzt den Schatten für UI Elemente
    public func shadow(){
        
        layer.shadowColor = UIColor(colorLiteralRed: Float(SHADOW_GRAY), green: Float(SHADOW_GRAY), blue: Float(SHADOW_GRAY), alpha: 0.6).cgColor
        layer.shadowOpacity = 0.4
        layer.shadowRadius = 3.0
        layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
    }

}
