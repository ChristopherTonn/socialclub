//
//  GardientView.swift
//  ios10SocialClub
//
//  Created by Christopher on 15.06.17.
//  Copyright © 2017 Student. All rights reserved.
//

import UIKit

@IBDesignable
class GardientView: UIView {

    
    @IBInspectable var FirstColor: UIColor = UIColor.clear{
        
        didSet{
            updateView()
        }
    }
    
    @IBInspectable var SecondColor: UIColor = UIColor.clear{
        
        didSet{
            updateView()
        }
    }

    
    @IBInspectable var HoriontalGardient: Bool = false{
        
        didSet{
            
            updateView()
        }
    }
    
    override class var layerClass: AnyClass{
        
        get{
            
            return CAGradientLayer.self
        }
    }
    
    func updateView(){
        
        let layer = self.layer as! CAGradientLayer
        layer.colors = [FirstColor.cgColor, SecondColor.cgColor]
        
        if(HoriontalGardient){
            
            layer.startPoint = CGPoint(x: 0.0, y: 0.5)
            layer.endPoint = CGPoint(x:1.0, y: 0.5)
            
        }else{
            
            layer.startPoint = CGPoint(x: 0, y: 0)
            layer.endPoint = CGPoint(x: 0, y: 1)
        }
    }

}
