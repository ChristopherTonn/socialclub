//
//  TabBarVC.swift
//  ios10SocialClub
//
//  Created by Christopher on 09.06.17.
//  Copyright © 2017 Student. All rights reserved.
//

import UIKit

class TabBarVC: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //Farbe von selectiertes simbol aendern
        self.tabBar.tintColor = UIColor.black
        //Farbe von tab bar background aendern
        //self.tabBar.barTintColor = UIColor.groupTableViewBackground.withAlphaComponent(0.1)
        //Farbe von standbuy simbol aendern
        self.tabBar.unselectedItemTintColor = UIColor.black
        
        
        
        
        
        
        let topBorder = CALayer()
       // topBorder.frame = CGRectMake(0,0,1000,0.5)
        topBorder.frame = CGRect(x:0,y:0,width:1000,height:0.5)
        topBorder.backgroundColor = UIColor.lightGray.withAlphaComponent(0.4).cgColor

        
        tabBar.layer.addSublayer(topBorder)
        tabBar.clipsToBounds = true
        //self.tabBar.isTranslucent = false
      //  self.tabBar.backgroundColor = UIColor.white.withAlphaComponent(0.2)
       // self.tabBarItem.image = UIImage(named: "homeOn")?.withRenderingMode(.alwaysOriginal)
        
        //self.tabBarItem.selectedImage = UIImage(named: "homeOff")?.withRenderingMode(.alwaysOriginal)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func tapped(sender: AnyObject) {
        
        // what goes here to show/hide the tabBar ???
        self.tabBarController?.tabBar.isHidden = true
        

        
    }
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


