//
//  Post.swift
//  ios10SocialClub
//
//  Created by Christopher on 26.04.17.
//  Copyright © 2017 Student. All rights reserved.
//

import Foundation
import Firebase

class Post {
    
    private var _caption: String!
    private var _userName: String!
    private var _imageUrl: String!
    private var _likes: Int!
    private var _postKey: String!
    private var _postRef: FIRDatabaseReference!
    
    var caption: String {
        
        return _caption
    }
    
    var userName: String {
        
        return _userName
    }
    
    var imageUrl: String {
        
        return _imageUrl
    }
    
    var likes: Int {
        
        return _likes
    }
    
    var postKey: String {
        
        return _postKey
    }
    
    
    init(caption: String, userName: String, imageUrl: String, likes: Int) {
        
        self._caption = caption
        self._userName = userName
        self._imageUrl = imageUrl
        self._likes = likes
    }
    
    init(postKey: String, postData: Dictionary<String, Any>) {
        
        self._postKey = postKey
        
        
        if let caption = postData["caption"] as? String{
            
            self._caption = caption
        }
        
        if let userName = postData["userName"] as? String{
            
            self._userName = userName
        }
        
        if let imageUrl = postData["imageUrl"] as? String{
            
            self._imageUrl = imageUrl
        }
        
        if let likes = postData["likes"] as? Int {
            
            self._likes = likes
        }
        
        //Fuer die Likes
        _postRef = DataService.ds.REF_POSTS.child(_postKey)
        
    }
    
    
    //Fuer die Likes
    func adjustLikes(addLike: Bool){
        
        if addLike{
            
            _likes = _likes + 1
        }
        else{
            
            _likes = likes - 1
        }
        
        //Diese Zeile updatet einen wert in der DB
        _postRef.child("likes").setValue(_likes)
        
    }
}
