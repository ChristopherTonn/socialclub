//
//  FeedVC.swift
//  ios10SocialClub
//
//  Created by Student on 03.04.17.
//  Copyright © 2017 Student. All rights reserved.
//

import UIKit
import Firebase
import SwiftKeychainWrapper

class FeedVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    //--Vars
    var posts = [Post]()
    
    
    static var imageCache: NSCache<NSString, UIImage> = NSCache()
    
    
    //---Outlets
    @IBOutlet weak var tableView: UITableView!
   

    
    //----Actions
    


    
    
    
    
    
    
    
    
    //Methods

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //Später für DB
        tableView.delegate = self
        tableView.dataSource = self
        
        
        //Posts werden aus Firebase geladen
        DataService.ds.REF_POSTS.queryOrdered(byChild: "postDate").observe(.value, with: { (snapshot) in
            
            self.posts = []
            
            if let snapshot = snapshot.children.allObjects as? [FIRDataSnapshot]{
                
                for snap in snapshot {
                    
                    if let postDict = snap.value as? Dictionary<String, Any> {
                        
                        let key = snap.key
                        let post = Post(postKey: key, postData: postDict)
                        self.posts.append(post)
                    
                    }
                }
                
                self.tableView.reloadData()
            }
            
        })
        
       
        setupTabBar()
    }
    
    

    
    
    
    //---Tab Bar Setup---
    
    private func setupTabBar(){
    
        //navigationController?.hidesBarsOnTap = true
    
    }
    

    
    //---Table View---
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //Ladt die Posts in Umgekehrter reinfolge runter
        let post = posts.reversed()[indexPath.row]

        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "CellView") as? CellView{
            
            
            //Speicher die runtergeladenen Images im Cache
            if let img = FeedVC.imageCache.object(forKey: post.imageUrl as NSString){
                
                cell.configureCell(post: post, img: img)
                return cell
            }
            else{
                
                cell.configureCell(post: post, img: nil)
                return cell
            }
            
            
        }
        else{
            
            print("Chris: CellView Error. 😱")
            return CellView()
        }
        
    }
    
    
        
}

extension UIViewController {
    
    func setTabBarVisible(visible: Bool, animated: Bool) {
        //* This cannot be called before viewDidLayoutSubviews(), because the frame is not set before this time
        
        // bail if the current state matches the desired state
        if (isTabBarVisible == visible) { return }
        
        // get a frame calculation ready
        let frame = self.tabBarController?.tabBar.frame
        let height = frame?.size.height
        let offsetY = (visible ? -height! : height)
        
        // zero duration means no animation
        let duration: TimeInterval = (animated ? 0.3 : 0.0)
        
        //  animate the tabBar
        if frame != nil {
            UIView.animate(withDuration: duration) {
                self.tabBarController?.tabBar.frame = frame!.offsetBy(dx: 0, dy: offsetY!)
                return
            }
        }
    }
    
    var isTabBarVisible: Bool {
        return (self.tabBarController?.tabBar.frame.origin.y ?? 0) < self.view.frame.maxY
    }
}
