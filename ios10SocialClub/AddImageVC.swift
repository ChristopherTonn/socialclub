//
//  ViewController.swift
//  ios10SocialClub
//
//  Created by Christopher on 04.05.17.
//  Copyright © 2017 Student. All rights reserved.
//

import UIKit
import Firebase

class AddImageVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate {
    
    
    
    //Vars
    var imagePicker = UIImagePickerController()
    var imageSelected = false
    
    var colorArray: [(color1: UIColor, color2: UIColor)] = []
    var currentColorArrayIndex = -1
    
    
    
    
    //Outlets
    
    @IBOutlet weak var imageAdd: UIImageView!
    @IBOutlet weak var captionField: UITextField!
    @IBOutlet weak var usernameField: UITextField!
    
    
//    @IBOutlet var animatedGardient: GardientView!
    @IBOutlet weak var animatedGardient: GardientView!
    
    
    
    //Actions
    @IBAction func addImageDow(_ sender: Any) {
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    
    @IBAction func addPost(_ sender: Any) {
        
        //Ueberpruefe ob du eine eingabe bekommen hast
        guard let caption = captionField.text, caption != "" else {
            
            print("Chris: Caption must be entered! ☝🏼")
            return
        }
        
        //Ueberpruefe ob ein Photo ausgewaehlt wurde
        guard let img = imageAdd.image, imageSelected == true else {
            
            print("Chris: An image must be selected! 😁")
            return
        }
        
        
        if let imgData = UIImageJPEGRepresentation(img, 1.2) {
            
            //Gebe hochgeladenen Photo eine Id
            let imgUid = NSUUID().uuidString
            let metaData = FIRStorageMetadata()
            metaData.contentType = "image/jpeg"
            
            DataService.ds.REF_POST_IMAGES.child(imgUid).put(imgData, metadata: metaData) { (metaData, error) in
                
                if error != nil {
                    
                    print("Chris: Unable to upload image to Firebase! Uahrr.. 🌨👵🏻")
                }
                else {
                    
                    print("Chris: Image successfully Upload to Firebase! Uhlala! 🌴💃🏾")
                    
                    let downloadURL = metaData?.downloadURL()?.absoluteString
                    
                    if let url = downloadURL{
                        self.postToFirebase(imgUrl: url)
                    }
                }
            }
        }
        
    }
    
    
    
    @IBAction func IBActionfunccancelBtn_senderAnycancelBtn(_ sender: Any) {
        print("Chris: Uff.. User canceld Image upload. 🙄")
        goToFeedVC()
        
    }
   
//    @IBAction func cancelBtn(_ sender: Any) {
//        
//        print("Chris: Uff.. User canceld Image upload. 🙄")
//        goToFeedVC()
//        
//    }
    
    
    
    //Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Image Picker
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        
        gardientColors()
        animateBackgroundColor()
        
        captionField.delegate = self
        usernameField.delegate = self
    }
    
    
    
    //---Image Picker
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        
        //Gebe das Edited Image als UIImage zurueck
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage{
            
            //Fuege es in die ImageView ein
            imageAdd.image = image
            //Wenn Image ausgewaehlt setze auf true
            imageSelected = true
        }
        else{
            
            print("Chris: Sh**! Its no Image was selected! 🤔")
        }
        
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    //Lade Post Hoch
    func postToFirebase(imgUrl: String){
        
        let post: Dictionary<String, Any> = ["caption": captionField.text!,
                                             "userName": usernameField.text!,
                                             "imageUrl": imgUrl,
                                             "likes": 0,
                                             "postDate": FIRServerValue.timestamp() ]
        
        
        let firebasePost = DataService.ds.REF_POSTS.childByAutoId()
        firebasePost.setValue(post)
        
        setAllFieldsToZero()
        goToFeedVC()
        
    }
    
    
    //Setze alle Felder wieder auf Ausgangspostion
    func setAllFieldsToZero(){
        
        captionField.text = ""
        imageSelected = false
        imageAdd.image = UIImage(named: "add-post")
    }
    
    
    //--- Keyboard Funktionen ---
    
    //wenn ich drausen drücke
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //wenn die returntaste gedrückt wird
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        captionField.resignFirstResponder()
        usernameField.resignFirstResponder()
        return true
    }
    
    //Bei Eingabe beginn elemente nach oben schieben
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        //  captionField.setContentOffSet(CGPointMake(0, 250), animated: true)
        moveTextField(textField: textField, moveDistance: -200, up: true)
    }
    
    //Bei Eingabe ende elemente nach unten schieben
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        moveTextField(textField: textField, moveDistance: -200, up: false)
    }
    
    //Elemente Animieren/Verschieben
    func moveTextField(textField: UITextField, moveDistance: Int, up: Bool){
        
        let moveDuration = 0.3
        let movement: CGFloat = CGFloat(up ? moveDistance : -moveDistance)
        
        UIView.beginAnimations("animatedTextField", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(moveDuration)
        
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        UIView.commitAnimations()
    }
    
    
    
    
    //Go to Feed VC
    func goToFeedVC (){
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "tabBarVc") as! TabBarVC
        self.present(vc, animated: true, completion: nil)
    }
    
    func gardientColors(){
        
        _ = #colorLiteral(red: 0.3647058904, green: 0.06666667014, blue: 0.9686274529, alpha: 1)
        colorArray.append(( color1: #colorLiteral(red: 0.4612618685, green: 0.7622496693, blue: 0.7422841787, alpha: 1), color2:#colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1) ))
        colorArray.append(( color1: #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1), color2:#colorLiteral(red: 0.3647058904, green: 0.06666667014, blue: 0.9686274529, alpha: 1) ))
        colorArray.append(( color1: #colorLiteral(red: 0.3647058904, green: 0.06666667014, blue: 0.9686274529, alpha: 1), color2:#colorLiteral(red: 0.5568627715, green: 0.3529411852, blue: 0.9686274529, alpha: 1) ))
        colorArray.append(( color1: #colorLiteral(red: 0.5568627715, green: 0.3529411852, blue: 0.9686274529, alpha: 1), color2:#colorLiteral(red: 0.5810584426, green: 0.1285524964, blue: 0.5745313764, alpha: 1) ))
        colorArray.append(( color1: #colorLiteral(red: 0.5810584426, green: 0.1285524964, blue: 0.5745313764, alpha: 1), color2:#colorLiteral(red: 0.4612618685, green: 0.7622496693, blue: 0.7422841787, alpha: 1) ))
    }
    
    
    //Animiert den Hintergrund
    func animateBackgroundColor(){
        
        currentColorArrayIndex = currentColorArrayIndex == (colorArray.count - 1) ? 0:currentColorArrayIndex + 1
        
        UIView.transition(with: animatedGardient, duration: 5, options: [.transitionCrossDissolve], animations: {
            
            self.animatedGardient.FirstColor = self.colorArray[self.currentColorArrayIndex].color1
            self.animatedGardient.SecondColor = self.colorArray[self.currentColorArrayIndex].color2
        }) { (success) in
            self.animateBackgroundColor()
        }
    }
    

//    //Animiert den Hintergrund
//    func animateBackgroundColor(){
//        
//        currentColorArrayIndex = currentColorArrayIndex == (colorArray.count - 1) ? 0:currentColorArrayIndex + 1
//        
//        UIView.transition(with: animatedGardient, duration: 5, options: [.transitionCrossDissolve], animations: {
//            
//            self.animatedGardient.FirstColor = self.colorArray[self.currentColorArrayIndex].color1
//            self.animatedGardient.SecondColor = self.colorArray[self.currentColorArrayIndex].color2
//        }) { (success) in
//            self.animateBackgroundColor()
//        }
//    }
    
}
