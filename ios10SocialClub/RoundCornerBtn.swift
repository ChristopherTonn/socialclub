//
//  RoundCornerBtn.swift
//  ios10SocialClub
//
//  Created by Christopher on 06.04.17.
//  Copyright © 2017 Student. All rights reserved.
//

import UIKit

class RoundCornerBtn: UIButton {
    
    override func layoutSubviews() {
        
        super.layoutSubviews()
        roundCorner()
        thinBorder()
    }
    
    //Runde Ecken
    public func roundCorner(){
        
        layer.cornerRadius = self.frame.width/12
        clipsToBounds = true
    }
    
    
    //Duenne Linie
    private func thinBorder(){
        
        self.layer.borderWidth  = 1.2
        self.layer.borderColor = UIColor.white.cgColor
        layer.masksToBounds = true
    }
    
}
