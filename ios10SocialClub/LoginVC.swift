//
//  LoginVC.swift
//  ios10SocialClub
//
//  Created by Student on 03.04.17.
//  Copyright © 2017 Student. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import Firebase
import SwiftKeychainWrapper





class LoginVC: UIViewController, UITextFieldDelegate {
    
    
    //Outlets
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    
    
    //Actions
    
    //Lege User an
    @IBAction func signIn(_ sender: Any) {
        
        if let mail = emailField.text, let pwd = passwordField.text {
            
            FIRAuth.auth()?.signIn(withEmail: mail, password: pwd, completion: { (user, error) in
                
                if error == nil{
                    
                    print("Chris: Successfully auth with Email in Firebase. Cooool! 😃")
                    
                    if let user = user{
                        
                        let userData = ["provider": user.providerID]
                        
                        self.finalSignIn(id: user.uid, userData: userData )
                    }
                    
                }
                else{
                    
                    FIRAuth.auth()?.createUser(withEmail: mail, password: pwd, completion: { (user, error) in
                        
                        if error != nil{
                            
                            print("Chris: Unable to auth with Firebase. :(")
                        }
                        else {
                            
                            print("Chris: Successfully auth in Firebase. :D")
                            
                            
                            if let user = user {
                                
                                let userData = ["provider": user.providerID]
                                self.finalSignIn(id: user.uid, userData: userData)
                            }
                        }
                    })
                }
            })
        }
        
    }
    
    
    //Lege User vi facebook email adresse an.
    @IBAction func facebookSignIn(_ sender: AnyObject) {
        
        let facebookLogin = FBSDKLoginManager()
        
        facebookLogin.logIn(withReadPermissions: ["email"], from: self ){ (result, error) in
            
            if error != nil{
                
                print("Chris: Unable to auth with Facebook.")
            }
            else if result?.isCancelled == true{
                
                print("Chris: User canceld Facebook Auth.")
            }
            else{
                
                print("Chris: Successfully auth with Facebook. JUHU")
                
                let credential = FIRFacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)
                self.firebaseAuth(credential)
            }
        }
    }
    
    //##### Methods ####
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        emailField.delegate = self
        passwordField.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        //Auto Login. Wenn User vorhanden gehe zu FeedVC bzw. TabBarVC
        if let _ = KeychainWrapper.standard.string(forKey: KEY_UID){
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "tabBarVc") as! TabBarVC
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    
    //Firebase
    func firebaseAuth(_ credential: FIRAuthCredential){
        
        FIRAuth.auth()?.signIn(with: credential, completion: { (user, error) in
            
            if error != nil {
                
                print("Chris: Unable to auth with Facebook.")
            }
            else{
                
                print("Chris: Successfully auth with Facebook. STRIKE!")
                
                if let user = user{
                    
                    let userData = ["provider": credential.provider]
                    self.finalSignIn(id: user.uid, userData: userData)
                }
            }
        })
    }
    
    
    //Skip to ViewController
    func finalSignIn(id: String , userData: Dictionary<String, String>){
        
        //Auto Login
        KeychainWrapper.standard.set(id, forKey: KEY_UID)
        print("Chris: Keychain saved Data. 👍🏼")
        
        //User in Datenbank erstellen
        DataService.ds.createFirebaseDBUser(uid: id, userData: userData)
        
        //Ziel Instanz erzeugen & dann sichtbar machen
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "tabBarVc") as! TabBarVC
        self.present(vc, animated: true, completion: nil)
    }
    
    
    
    //--- Keyboard verschwinde! ---
    
    //wenn ich drausen drücke
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //wenn die returntaste gedrückt wird
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return (true)
    }
    
    
    //Bei Eingabe beginn elemente nach oben schieben
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        //  captionField.setContentOffSet(CGPointMake(0, 250), animated: true)
        moveTextField(textField: textField, moveDistance: -100, up: true)
    }
    
    //Bei Eingabe ende elemente nach unten schieben
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        moveTextField(textField: textField, moveDistance: -100, up: false)
    }
    
    //Elemente Animieren/Verschieben
    func moveTextField(textField: UITextField, moveDistance: Int, up: Bool){
        
        let moveDuration = 0.3
        let movement: CGFloat = CGFloat(up ? moveDistance : -moveDistance)
        
        UIView.beginAnimations("animatedTextField", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(moveDuration)
        
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        UIView.commitAnimations()
    }
    
    
    
    
}
