//
//  CircleView.swift
//  ios10SocialClub
//
//  Created by Christopher on 03.04.17.
//  Copyright © 2017 Student. All rights reserved.
//

import UIKit

class CircleView: UIImageView {
    
    
   
    override func layoutSubviews() {
        
        circle()
    }
    
    //View wird zu eine Kreis geformt
    
    public func circle(){
        
        layer.cornerRadius = self.frame.width/2
        
       
        self.layer.borderWidth  = 1.5
        self.layer.borderColor = UIColor.white.cgColor
        layer.masksToBounds = true
        //clipsToBounds = true
    }
    
}
