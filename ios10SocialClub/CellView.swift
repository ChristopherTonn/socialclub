//
//  CellView.swift
//  ios10SocialClub
//
//  Created by Christopher on 04.04.17.
//  Copyright © 2017 Student. All rights reserved.
//

import UIKit
import Firebase

class CellView: UITableViewCell {
    
    
    //Vars
    var post: Post!
    var likesRef: FIRDatabaseReference!
    
    //Outlets
    @IBOutlet weak var profileImg:UIImageView!
    @IBOutlet weak var usernameLbl:UILabel!
    @IBOutlet weak var postImg:UIImageView!
    @IBOutlet weak var caption:UITextView!
    @IBOutlet weak var lovesLbl:UILabel!
    @IBOutlet weak var likeImg: UIImageView!
    
    //Actions
    
    
    //Methods
    
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        
        
        //Fuer Tap Reconigzer
        let tap = UITapGestureRecognizer( target: self, action: #selector(likeTapped) )
        tap.numberOfTapsRequired = 1
        likeImg.addGestureRecognizer(tap)
        likeImg.isUserInteractionEnabled = true
        
    }
    
    func likeTapped(sender: UITapGestureRecognizer){
        
        
        
        likesRef.observeSingleEvent(of: .value, with: { (snapshot) in
            
            if let _ = snapshot.value as? NSNull {
                
                print("Chris: Huhu! You touched my heart! ❤️👇🏼")
                self.likeImg.image = UIImage(named: "filledHeart")
                self.post.adjustLikes(addLike: true)
                self.likesRef.setValue(true)
            }
            else{
                
                print("Chris: NOOOO! Why do you break up? 💔👇🏼")
                self.likeImg.image = UIImage(named: "emptyHeart")
                self.post.adjustLikes(addLike: false)
                self.likesRef.removeValue()
            }
        })
        
       
    }
    
    
    
    
    
    func configureCell(post: Post, img: UIImage? = nil){
        
        self.post = post
        
        //likesRef = DataService.ds.REF_USER_CURRENT.child("likes").child(post.postKey)
        likesRef = DataService.ds.REF_USER_CURRENT.child("likes").child(post.postKey)

        self.caption.text = post.caption
        self.usernameLbl.text = post.userName
        self.lovesLbl.text = "\(post.likes)"
        
        if img != nil{
            
            self.postImg.image = img
        }
        else{
            
            let ref = FIRStorage.storage().reference(forURL: post.imageUrl)
            ref.data(withMaxSize: 2 * 1024 * 1024, completion: { (data, error) in
                
                if error != nil{
                    
                    print("Chris: Unable to download Image. 😯")
                }
                else{
                    
                    print("Chris: Image downloaded from storage. Nice! 😀")
                    if let imgData = data {
                        if let img = UIImage(data: imgData) {
                            self.postImg.image = img
                            FeedVC.imageCache.setObject(img, forKey: post.imageUrl as NSString)
                        }
                    }
                }
            })
          
        }
        
        likesRef.observeSingleEvent(of: .value, with: { (snapshot) in
            
            if let _ = snapshot.value as? NSNull {
                
                self.likeImg.image = UIImage(named: "emptyHeart")
            }
            else{
                
                self.likeImg.image = UIImage(named: "filledHeart")
            }
        })
    }
    
        
    


}
