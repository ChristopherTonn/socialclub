
//  DataService.swift
//  ios10SocialClub
//
//  Created by Christopher on 24.04.17.
//  Copyright © 2017 Student. All rights reserved.
//

import Foundation
import Firebase
import SwiftKeychainWrapper


let DB_Base = FIRDatabase.database().reference() //Url zur Datenbank
let STORAGE_Base = FIRStorage.storage().reference()

class DataService {
    
    static let ds = DataService()
    
    //DB Referenzen
    private var _REF_BASE = DB_Base
    private var _REF_POSTS = DB_Base.child("posts")
    private var _REF_USERS = DB_Base.child("users")
   
    
    private var _userRef: FIRDatabaseReference!
    
    //Storage Referenzen
    private var _REF_POST_IMAGES = STORAGE_Base.child("post-image")
    
    private var _REF_PROFILE_IMAGE = STORAGE_Base.child("profile-image")
    
    
    
    var REF_BASE: FIRDatabaseReference {
        return _REF_BASE
    }
    
    var REF_POSTS: FIRDatabaseReference {
        return _REF_POSTS
    }
    
    var REF_USERS: FIRDatabaseReference {
        return _REF_USERS
    }
    
    var REF_USER_CURRENT: FIRDatabaseReference {
        //let uid = KeychainWrapper.stringForKey(KEY_UID)
        //let uid = KeychainWrapper.set(KEY_UID)
        let uid = KeychainWrapper.defaultKeychainWrapper.string(forKey: KEY_UID)
        let user = REF_USERS.child(uid!)
        return user
    }
    
    var REF_POST_IMAGES: FIRStorageReference {
        return _REF_POST_IMAGES
    }
    
    var REF_PROFILE_IMAGE: FIRStorageReference {
        return _REF_PROFILE_IMAGE
    }
    
    func createFirebaseDBUser(uid: String, userData: Dictionary <String, String>){
        
        REF_USERS.child(uid).child("username").updateChildValues(userData)
    }
    
    
}
