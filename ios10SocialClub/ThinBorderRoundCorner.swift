//
//  ThinBorderRoundCorner.swift
//  ios10SocialClub
//
//  Created by Christopher on 14.06.17.
//  Copyright © 2017 Student. All rights reserved.
//

import UIKit

class ThinBorderRoundCorner: UIView {

    override func layoutSubviews() {
        
        super.layoutSubviews()
        thinBorder()
        roundCorner()
        shadow()
    }
    
    //Duenne Linie
    private func thinBorder(){
        
        self.layer.borderWidth  = 1.4
        self.layer.borderColor = UIColor.lightGray.cgColor
        layer.masksToBounds = true
    }
    
    //Runde Ecken
    private func roundCorner(){
        
        layer.cornerRadius = self.frame.width/12
        clipsToBounds = true
    }

    //Schatten
    public func shadow(){
        
        layer.shadowColor = UIColor(colorLiteralRed: Float(SHADOW_GRAY), green: Float(SHADOW_GRAY), blue: Float(SHADOW_GRAY), alpha: 0.6).cgColor
        layer.shadowOpacity = 0.9
        layer.shadowRadius = 6.0
        layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
    }


}
