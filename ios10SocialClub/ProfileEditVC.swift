//
//  ProfileEditVC.swift
//  ios10SocialClub
//
//  Created by Christopher on 12.06.17.
//  Copyright © 2017 Student. All rights reserved.
//

import UIKit
import Firebase
import SwiftKeychainWrapper

class ProfileEditVC: UIViewController {
    
    //Vars
    var colorArray: [(color1: UIColor, color2: UIColor)] = []
    var currentColorArrayIndex = -1
    
    
    //Outlets
    
    @IBOutlet weak var animatedGardient: GardientView!
    
    
    @IBAction func logoutBtnDown(_ sender: Any) {
        
        print("Chris: Hey! Im out! 👋🏼")
        
        //Keychain Log Out
        KeychainWrapper.standard.removeObject(forKey: KEY_UID)
        
        //Firebase Log Out
        try! FIRAuth.auth()?.signOut()
        
        
        //Erst Ziel Instanz erzeugen & dann sichtbar machen
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "loginVC") as! LoginVC
        self.present(vc, animated: true, completion: nil)
        //performSegue(withIdentifier: "loginVC", sender: nil)
        
    }
    
    @IBAction func logoutBtnDown1(_ sender: Any) {
        
        print("Chris: Hey! Im out! 👋🏼")
        
        //Keychain Log Out
        KeychainWrapper.standard.removeObject(forKey: KEY_UID)
        
        //Firebase Log Out
        try! FIRAuth.auth()?.signOut()
        
        
        //Erst Ziel Instanz erzeugen & dann sichtbar machen
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "loginVC") as! LoginVC
        self.present(vc, animated: true, completion: nil)
    }
    
    
    
    //Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        gardientColors()
        animateBackgroundColor()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func gardientColors(){
        
        _ = #colorLiteral(red: 0.3647058904, green: 0.06666667014, blue: 0.9686274529, alpha: 1)
        colorArray.append(( color1: #colorLiteral(red: 0.4612618685, green: 0.7622496693, blue: 0.7422841787, alpha: 1), color2:#colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1) ))
        colorArray.append(( color1: #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1), color2:#colorLiteral(red: 0.3647058904, green: 0.06666667014, blue: 0.9686274529, alpha: 1) ))
        colorArray.append(( color1: #colorLiteral(red: 0.3647058904, green: 0.06666667014, blue: 0.9686274529, alpha: 1), color2:#colorLiteral(red: 0.5568627715, green: 0.3529411852, blue: 0.9686274529, alpha: 1) ))
        colorArray.append(( color1: #colorLiteral(red: 0.5568627715, green: 0.3529411852, blue: 0.9686274529, alpha: 1), color2:#colorLiteral(red: 0.5810584426, green: 0.1285524964, blue: 0.5745313764, alpha: 1) ))
        colorArray.append(( color1: #colorLiteral(red: 0.5810584426, green: 0.1285524964, blue: 0.5745313764, alpha: 1), color2:#colorLiteral(red: 0.4612618685, green: 0.7622496693, blue: 0.7422841787, alpha: 1) ))
    }
    
    func animateBackgroundColor(){
        
        currentColorArrayIndex = currentColorArrayIndex == (colorArray.count - 1) ? 0:currentColorArrayIndex + 1
        
        UIView.transition(with: animatedGardient, duration: 5, options: [.transitionCrossDissolve], animations: {
            
            self.animatedGardient.FirstColor = self.colorArray[self.currentColorArrayIndex].color1
            self.animatedGardient.SecondColor = self.colorArray[self.currentColorArrayIndex].color2
        }) { (success) in
            self.animateBackgroundColor()
        }
    }
    
}
